# Spring Boot basic security with AngularJs

### Introduction

Basic security is one of the easiest way implement authentication. Here is an example of how the basic authentication works in spring boot. In this tutorial I am going to explain how we can create a single page page application with spring boot security and angularjs. Let’s get started.

You can see the WebConfig.java class inside org.techforumist.basic.security package. It would be better if you create a new package for all configuration related to the web application. For this tutorial let it be there.

**@Configurable** used help the spring container to find that it is a configuration class.
**@EnableWebSecurity** annotation says that we are going to override the default security configurations.

The **WebConfig** class is extending from **WebSecurityConfigurerAdapter** class, which is an Adapter class that extending from **WebSecurityConfigurer**. So it will create an instance of **WebSecurityConfigurer** which is used in the spring security module for implementing the security.

As you can see inside the **WebConfig** class there are three method which are overriding the default behavior of the **WebSecurityConfigurer** class.

1. configure(**AuthenticationManagerBuilder** auth)
This method is for overriding the default AuthenticationManagerBuilder instance in the container. Using AuthenticationManagerBuilder instance we can specify how the user credentials are kept in the web application. It may be in database, LDAP or in Memory. Here we are using the in memory authentication. As you can see in the code I am adding two users in the application. An administrator user and normal user.

2. configure(**WebSecurity** web)
This method is for overriding some configuration of the WebSecurity instance in the container. If you want to ignore some request or request patterns then you can specify that inside this method. For example if you want to ignore all the request starting from “/contents” from security then we can use the following lines of code.

```
web
      .ignoring()
         .antMatchers("/contents/**");
```

3. configure(**HttpSecurity** http)
This method is used for override HttpSecurity instance of the web Application. We can specify our authorization criteria inside this method. See the inline comments in the above code. One thing you have to notice that we are setting the session creation policy as STATELESS. Which means there is no session in the server.