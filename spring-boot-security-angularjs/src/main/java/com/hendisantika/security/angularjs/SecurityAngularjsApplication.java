package com.hendisantika.security.angularjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityAngularjsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityAngularjsApplication.class, args);
	}
}
