package com.hendisantika.security.angularjs;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hendisantika on 6/9/17.
 */
@RestController
@RequestMapping("/api/admin")
public class AdminRestController {
    @RequestMapping("/resource")
    public Map<String, Object> sayHello() {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Hello Administrator");
        map.put("timestamp", new Date().getTime());
        return map;
    }
}
