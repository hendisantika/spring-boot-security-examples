package com.hendisantika.security.angularjs;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by hendisantika on 6/9/17.
 */
@RestController
public class AppRestController {

    @RequestMapping("/user")
    public Principal sayHello(Principal principal) {
        return principal;
    }
}
