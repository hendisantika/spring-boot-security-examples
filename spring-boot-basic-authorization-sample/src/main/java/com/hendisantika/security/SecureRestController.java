package com.hendisantika.security;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hendisantika on 6/9/17.
 */
@RestController
public class SecureRestController {
    @RequestMapping("/sayHello")
    public String sayHello() {
        return "Hello World!";
    }
}
